![JWT-Auth for Laravel](https://lh3.googleusercontent.com/Ou4PcdLdj7T8QEeGGWGOvGV10iCnbF0mZ_D7TdGSGwWvl8NI4DfCeySISEfa7R5gSlHRDaFVsYzTuQ=w1920-h949-rw)
# JWT-Auth for Laravel

## Installation

Package is available on [Packagist](http://packagist.org/packages/antarccub/jwt-auth),  
you can install it using [Composer](http://getcomposer.org).  
  
```shell  
composer require antarccub/jwt-auth  
```  
  
After that, you need to publish config file with the following command:  
```shell  
php artisan vendor:publish --tag:jwt-config  
```  
  
To start using the package you have to change your auth driver in the auth.php file:  
  
```php  
'guards' => [  
 'web' => [ 'driver' => 'session', 'provider' => 'users', ],  
 'api' => [ 'driver' => 'jwt', // JWT Auth 'provider' => 'users', ], ],  
```  
  
  

## Basic Usage

  
  
Now you can use Laravel Auth facade with JWT Authentication  
  
```php  
Route::middleware('auth:api')->get('/user', function (Request $request) {  
  
  return response()->json(Auth::user());  
}); 
```  
  

## Configuration

  
  
```php  
/*  
 * Here you can add all claims (iss, aud, jti, sub) you want to validate and theirs corresponding value * * Example: *  'iss' => 'http://example.com', * *  'jti' => '123123j2j3oj' * */  
 'claims' => [  
 // 'iss' => 'http://example.com',  
 ],  
 /* * Here you can config the signature verification type * */  
 'signature' => 'public-key', // public-key | secret  
  
 'methods' => [ 'public-key' => [ 'type' => 'RSA', // RSA, ECDSA 'signer' => 'SHA256', // RSA256, RSA348, RSA512 'provider' => 'Antarccub\JwtAuth\Providers\PublicKeyFileProvider',  
 'content' => storage_path('public.key'), // Only for PublicKeyFileProvider 'url' => env('JWT_PKEY') // Only for PublicKeyUrlProvider ],  
 'secret' => [ 'signer' => 'SHA256', 'value' => 'example' ] ]
 ```  
  

## Contributing

 
  
This package is for my personal use, but feel free to change all you want!  
  
## References  
* [Laravel Documentation](https://laravel.com/docs)  
* [lcobucci/jwt](https://github.com/lcobucci/jwt)
