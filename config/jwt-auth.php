<?php

return [

    /*
     * Here you can add all claims (iss, aud, jti, sub) you want to validate and theirs corresponding value
     *
     * Example:
     *  'iss' => 'http://example.com',
     *
     *  'jti' => '123123j2j3oj'
     *
     */

    'claims' => [

        // 'iss' => 'http://example.com',

    ],

    /*
     * Here you can config the signature verification type
     *
     */

    'signature' => 'public-key', // public-key | secret


    'methods' => [
        'public-key' => [
            'type' => 'RSA', // RSA, ECDSA
            'signer' => 'SHA256', // RSA256, RSA348, RSA512
            'provider' => 'Antarccub\JwtAuth\Providers\PublicKeyFileProvider',

            'content' => storage_path('public.key'), // Only for PublicKeyFileProvider
            'url' => env('JWT_PKEY') // Only for PublicKeyUrlProvider
        ],

        'secret' => [
            'signer' => 'SHA256',
            'value' => 'example'
        ]
    ]





];