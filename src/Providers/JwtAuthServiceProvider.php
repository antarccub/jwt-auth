<?php

namespace Antarccub\JwtAuth\Providers;


use Antarccub\JwtAuth\Guards\JwtGuard;
use Illuminate\Auth\RequestGuard;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class JwtAuthServiceProvider extends ServiceProvider
{

    public function register(){

        $this->publishes([
            __DIR__.'/../../config/jwt-auth.php' => config_path('jwt-auth.php')
        ], 'jwt-config');

        $this->registerGuard();
    }

    /**
     * Register the token guard.
     *
     * @return void
     */
    protected function registerGuard()
    {
        Auth::extend('jwt', function ($app, $name, array $config) {
            return tap($this->makeGuard($config), function ($guard) {
                $this->app->refresh('request', $guard, 'setRequest');
            });
        });
    }

    /**
     * Make an instance of the token guard.
     *
     * @param  array  $config
     * @return \Illuminate\Auth\RequestGuard
     */
    private function makeGuard(array $config){

        return new RequestGuard(function ($request) use ($config) {
            return (new JwtGuard())->user($request);
        }, $this->app['request']);
    }


}