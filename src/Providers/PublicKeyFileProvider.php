<?php

namespace Antarccub\JwtAuth\Providers;


use Antarccub\JwtAuth\Contracts\PublicKeyProviderContract;
use GuzzleHttp\Client;
use Lcobucci\JWT\Signer\Key;

class PublicKeyFileProvider implements PublicKeyProviderContract
{

    public function getKey()
    {
        return new Key($this->content());

    }


    public function content(){

        return file_get_contents(config('jwt-auth.methods.public-key.content'));

    }
}