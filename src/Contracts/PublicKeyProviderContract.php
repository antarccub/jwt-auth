<?php

namespace Antarccub\JwtAuth\Contracts;


use Lcobucci\JWT\Signer\Key;

interface PublicKeyProviderContract
{

    /**
     * @return Key
     */
    public function getKey();

}