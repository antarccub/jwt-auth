<?php

namespace Antarccub\JwtAuth;


use Antarccub\JwtAuth\Traits\JwtAuthenticatable;
use Antarccub\JwtAuth\Traits\Serializable;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Support\Jsonable;
use JsonSerializable;
use Lcobucci\JWT\Token;

class User implements Authenticatable, Jsonable, JsonSerializable
{

    use JwtAuthenticatable, Serializable;

    protected $token;


    public function __construct(Token $jwtToken)
    {
        $this->token = $jwtToken;

        $this->attributes['id'] = $this->token->getClaim('sub');
        $this->attributes['email'] = $this->token->getClaim('email');


    }

    public function token(){
        return $this->token;
    }


}