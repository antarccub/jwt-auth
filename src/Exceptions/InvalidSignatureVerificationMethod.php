<?php

namespace Antarccub\JwtAuth\Exceptions;



class InvalidSignatureVerificationMethod extends \Exception
{
    public function __construct($signer)
    {
        parent::__construct("The signature verification settings are not available: ".$signer);
    }
}