<?php


namespace Antarccub\JwtAuth\Guards;


use Antarccub\JwtAuth\Exceptions\InvalidSignatureVerificationMethod;
use Antarccub\JwtAuth\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Request;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Token;
use Lcobucci\JWT\ValidationData;

class JwtGuard
{


    /**
     * Get the currently authenticated user.
     *
     * @param Request $request
     * @return Authenticatable|null
     * @throws InvalidSignatureVerificationMethod
     */
    public function user(Request $request)
    {
        if($request->bearerToken()){
            return $this->authenticateViaBearerToken($request);
        }

    }

    /**
     * @param Request $request
     * @return User
     * @throws InvalidSignatureVerificationMethod
     */
    public function authenticateViaBearerToken(Request $request){

        try{
            $token = (new Parser())->parse((string)  $request->bearerToken()); // Parses from a string
        }catch (\RuntimeException $exception){
            return null;
        }


        if($this->securityCheck($token)){
            return new User($token);
        }

    }

    /**
     * @param Token $token
     * @return bool
     * @throws InvalidSignatureVerificationMethod
     */
    public function securityCheck(Token $token){


        return $token->validate($this->validationRules())
            && $this->tokenVerified($token);
    }


    private function validationRules(){

        $validationRules = new ValidationData(); // It will use the current time to validate (iat, nbf and exp)

        foreach (config('jwt-auth.claims') as $claim => $value){

            switch ($claim){
                case 'iss':
                    $validationRules->setIssuer($value);
                    break;

                case 'aud':
                    $validationRules->setAudience($value);
                    break;

                case 'jti':
                    $validationRules->setId($value);
                    break;

                case 'sub':
                    $validationRules->setSubject($value);
                    break;

                default:
                    break;
            }
        }
        return $validationRules;
    }

    /**
     * @param Token $token
     * @return bool
     * @throws InvalidSignatureVerificationMethod
     */
    private function tokenVerified(Token $token){

        $signature = config('jwt-auth.signature');

        $method =  config('jwt-auth.methods')[$signature];
        $signer = title_case($method['signer']);


        switch ($signature){
            case 'public-key':
                $signerNamespace = "\Lcobucci\JWT\Signer\\".title_case($method['type'])."\\".$signer;
                $verificationValue = $this->getPublicKeyProvider()->getKey();
                break;
            case 'secret':
                $signerNamespace = "\Lcobucci\JWT\Signer\Hmac\Sha".$signer;
                $verificationValue = $method['value'];

                break;
            default:
                $signerNamespace = "\Lcobucci\JWT\Signer\Hmac\Sha".$signer;
                $verificationValue = $method['value'];
                break;
        }


        if(class_exists($signerNamespace)){
            return $token->verify(new $signerNamespace, $verificationValue);
        }else{
            throw new InvalidSignatureVerificationMethod($signerNamespace);
        }
    }

    /**
     * @return \Antarccub\JwtAuth\Contracts\PublicKeyProviderContract
     */
    private function getPublicKeyProvider(){

        $publicKeyProviderClass = config('jwt-auth.methods.public-key.provider');

        return new $publicKeyProviderClass;
    }


}